<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use App\Post;
use App\User;
use App\Komentar;
use File;
use Auth;


class DashboardController extends Controller
{
    public function index()
    {
        $listpost = Post::orderBy('id', 'DESC')->get();
        $totaluser = User::all()->count();
        $totalpost = Post::all()->count();
        $totalkomen = Komentar::all()->count();
        
        //dd($profile);
        return view('dashboard', compact('listpost','totaluser','totalpost','totalkomen'));
    }
}
