<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use App\Post;
use App\User;
use App\Komentar;
use File;
use Auth;

class KomentarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $post = Post::where('id', $id)->first();
        return view('komentar.create',compact('post'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'isi' => 'required',
        ]);

        $user_id = Auth::user()->id;
        $post_id = $request->post_id;

        Komentar::create([
            'user_id' => $user_id,
            'post_id' => $post_id,
            'isi' => $request->isi,
            'komentar_like' => '0'
        ]);
        
        return redirect("/komentar/create/".$post_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $post = Post::where('id', $id)->first();

        return view('komentar.show',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $komentar = Komentar::where('id', $id)->first();
        $post = Post::where('id', $komentar->post_id)->first();

        return view('komentar.edit',compact('post', 'komentar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $this->validate($request,[
            'isi' => 'required',
        ]);

        $komentar = Komentar::where('id', $request->komentar_id)->first();
        $komentar->isi = $request->isi;
        $komentar->save();
        
        return redirect("/komentar/create/".$komentar->post_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deletedRows = Komentar::where('id', $id)->delete();
        return redirect('/');
    }
}
