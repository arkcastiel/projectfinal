<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Profile;
use App\Post;
use App\User;
use File;
use Auth;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listpost = Post::where('user_id', Auth::user()->id)->get();
        return view('post.index', compact('listpost'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  dd($request);
        // //validator
        // $this->validate($request, [
        //     'isi' => 'required',
        //     'gambar' => 'mimes:jpeg,jpg,png|max:2200'
        // ]);

        // Get session user
        $user_id = Auth::user()->id;

        // Cek gambar
        if ($request->hasFile('gambar')) {
            $destinationPath = 'assets/image';
            $file = $request->file('gambar'); // will get all files
            $file_name = $file->getClientOriginalName(); //Get file original name
            $file->move($destinationPath , $file_name); // move files to destination folder
        }else{
            $file_name = "";
        }   

        // Create to Database
        $new_post = Post::create
        ([
            'isi' => $request->isi,
            'like' => 0,
            'gambar'  => $file_name,
            'slug'  => Str::slug($request['isi'], '-'), // Make Slug Link
            'user_id' => $user_id
        ]);

        //dd($new_post);
        // $new_gambar->move('assets/image', $new_gambar);
        return redirect('/post');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findorfail($id);
        // dd($post);
        return view('post.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'isi' => 'required'
        ]);

        $post = Post::findorfail($id);
        
        $post_data = [
            'isi' => $request->isi,
            ];

        $post->update($post_data);

        return redirect('/post');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //LIKE
    public function like(Request $request, $id)
    {
        
        $post = Post::where('id', $id)->first();
        $post->like = $post->like + 1;
        $post->save();

        return redirect('/');
    }
}
