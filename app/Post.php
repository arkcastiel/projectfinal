<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'post';
    protected $fillable = ['isi', 'like','gambar','slug', 'user_id'];

    // One to Many dengan Komentar Pertanyaan
    public function komentar(){
        return $this->hasMany('App\Komentar');
    }

    // Many to One dengan User
    public function user(){
        return $this->belongsTo('App\User');
    }

}
