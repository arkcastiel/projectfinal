<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/', 'DashboardController@index');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('komentar/show/{post_id}', 'KomentarController@show')->name('komentar.show');

Route::group(['middleware' => 'auth'], function () {
    //Profile
    Route::resource('profile', 'ProfileController')->only(['index','update','store']);
    // Post
    Route::resource('post', 'PostController');    
    Route::post('postlike/{post_id}', 'PostController@like')->name('komentar.like');
    
    // Komentar
    Route::get('komentar/create/{post_id}', 'KomentarController@create')->name('komentar.create');
    Route::get('komentar/{post_id}/edit', 'KomentarController@edit')->name('komentar.edit');
    Route::post('komentar/store', 'KomentarController@store')->name('komentar.store');
    Route::delete('/komentar/{komentar_id}', 'KomentarController@destroy');

    Route::put('komentar/update', 'KomentarController@update')->name('komentar.update');
});





