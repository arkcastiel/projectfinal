@extends('layout.master')

@section('title')
    Selamat Datang !
@endsection

@section('content')

@if ($profil != null)
<form action="/profile/{{$profil->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('put')

    <div class="form-group">
        <label for="bio">bio</label>
        <input type="text" class="form-control" value="{{$profile->bio}}" name="bio" id="bio" placeholder="Masukkan Bio">
        @error('bio')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="alamat">alamat</label>
        <input type="text" class="form-control" value="{{$profile->alamat}}" name="alamat" id="alamat" placeholder="Masukkan Alamat">
        @error('alamat')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="umur">umur</label>
        <input type="text" class="form-control" value="{{$profile->umur}}" name="umur" id="umur" placeholder="Masukkan Umur">
        @error('umur')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Edit</button>  
</form>

@else

<form action="/profil" method="POST" enctype="multipart/form-data" role="form" class="form-horizontals">
    @csrf

    <div class="form-group">
        <label for="bio">Bio</label>
        <input type="text" class="form-control" name="bio" id="bio" placeholder="Masukkan Biodata">
        @error('bio')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="alamat">Alamat</label>
        <input type="text" class="form-control" name="alamat" id="alamat" placeholder="Masukkan alamat">
        @error('umur')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="umur">Usia</label>
        <input type="text" class="form-control" name="umur" id="umur" placeholder="Masukkan Usia">
        @error('umur')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    
    <button type="submit" class="btn btn-primary">Tambah</button>  
</form>

@endif

@endsection