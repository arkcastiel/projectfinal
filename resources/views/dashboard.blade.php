@extends('layout.master')

@section('title')
   Dashboard
@endsection

@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                Total Postingan </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{$totalpost}}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-chat fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                Total User terdaftar </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{$totaluser}}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-chat fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                Total Komentar  </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{$totalkomen}}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-chat fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary"><i class="fa fa-fire"></i> Postingan Teratas</h6>
                    
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    @foreach ($listpost as $item)
                    <div class="card border-left-primary">
                        <div class="card-body">
                            <h3><b>{{ $item->user->name}} :</b></h3>  
                            <p>{{ $item->isi }}</p>
                            @if($item->gambar !=null)
                            <img src="{{asset('assets/image/'.$item->gambar)}}" style="width: 200px; height: 200px;" />
                            <br>
                            asdasdsads
                            <br>
                            @endif
                            <small> {{$item->created_at->diffForHumans()}} | <b>Like :</b> {{ $item->like }} | </small>
                            @auth
                            <a href="/komentar/create/{{ $item->id }}" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i> Berikan Komentar</a>
                            <form action='/postlike/{{$item->id}}' method="POST" style="display: inline">
                            <button type="submit" class="btn btn-primary btn-sm"> Berikan Like</button>
                            {{-- <a href="/post/like/{{ $item->id }}" class="btn btn-sm btn-info">Berikan Like</a>  --}}
                            @endauth
                        </div>
                    </div>
                        <span><b>Komentar : </b></span>
                        @foreach($item->komentar as $item_komentar)
                        <div class="alert alert-info" role="alert">
                        {{ $item_komentar->isi }}
                        <br>
                        <small><i>Oleh : {{ $item_komentar->user->name}}</i></small>
                        </div>
                        @endforeach
                    <hr>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection

