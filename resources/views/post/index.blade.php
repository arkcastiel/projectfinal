@extends('layout.master')

@section('title')
    Share Something
@endsection

@section('content')
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary"><i class="fa fa-fire"></i> Your Mind</h6>
                
            </div>
            <!-- Card Body -->
            <div class="card-body">
                <a class="btn btn-primary btn-md" href="/post/create"><i class="fa fa-edit"></i> Share Something Else</a>
                <br>
                <br>
                @foreach ($listpost as $item)
                @if ($item->gambar !=null)
                    <div class="card border-left-primary">
                        <div class="card-body">
                            <p>{{ $item->isi }}</p>
                            <img src="{{asset('assets/image/'.$item->gambar)}}" style="width: 200px; height: 200px;" />
                            <br>
                            <br>
                            <small>{{$item->created_at->diffForHumans()}} | <b>Like :</b> {{ $item->like }} | <b>Komentar </b> | <a href="/post/{{$item->id}}/edit" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i> Edit Post</a> </small>
                        </div>
                    </div> 
                    <hr>
                @else
                <div class="card border-left-primary">
                    <div class="card-body">
                        <p>{{ $item->isi }}</p>
                        <small> {{$item->created_at->diffForHumans()}} |<b>Like :</b> {{ $item->like }} | <b>Komentar </b> | <a href="/post/{{$item->id}}/edit" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i> Edit Post</a> </small>
                    </div>
                </div> 
                <br>
                @endif
                @endforeach
            </div>
        </div>        
@endsection
