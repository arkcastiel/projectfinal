@extends('layout.master')

@section('title')
    Whats on your mind ?
@endsection

@section('content')
 <div class="card shadow mb-4">
    <!-- Card Header - Dropdown -->
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary"><i class="fa fa-fire"></i> Create Posting</h6>
    </div>
    <!-- Card Body -->
    <div class="card-body">
        <form action="/post" method="POST" enctype="multipart/form-data">
        @csrf
          <div class="form-group">
            <label for="isi">Isi Posting</label>
            <textarea class="form-control" id="isi" name="isi" rows="5"></textarea> 
            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
          </div>
          <div class="form-group">
            <label for="gambar">Gambar</label>
            <input type="file" class="form-control" id="gambar" name="gambar">
          </div>
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@endsection

