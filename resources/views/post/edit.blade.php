@extends('layout.master')

@section('title')
    Whats on your mind ?
@endsection

@section('content')
      <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary"><i class="fa fa-fire"></i> Create Posting</h6>
        </div>
        <!-- Card Body -->
        <div class="card-body">
            <form action="/post/{{$post->id}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('put')
              <div class="form-group">
                <label for="isi">Isi Posting</label>
                <textarea class="form-control" id="isi" name="isi" rows="5">{{$post->isi}}</textarea> 
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                @error('isi')
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    {{ $message }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                @enderror
              </div>
              <button type="submit" class="btn btn-primary">Edit</button>
            </form>
        </div>
      </div>
@endsection
