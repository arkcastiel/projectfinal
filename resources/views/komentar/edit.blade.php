@extends('layout.master')

@section('title')
   Dashboard
@endsection

@section('content')
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary"><i class="fa fa-fire"></i>Posting</h6>              
                    </div>
                    <div class="card-body">
                        <div class="card border-left-primary">
                            <div class="card-body">
                                <p>{{ $post->isi }}</p>
                            </div>
                        </div>
                        <span><b>Komentar :</b></span> 
                        @if($post->komentar)
                            @foreach($post->komentar as $item_komentar)
                            <div class="alert alert-info" role="alert">
                              {{ $item_komentar->isi }}
                              <br>
                              <small><i>Oleh : {{ $item_komentar->user->name}}</i></small>
                            </div>
                            @endforeach
                        @else
                        <span><i>Belum ada Komentar</i></span>
                        @endif
                    </div>   
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary"><i class="fa fa-edit"></i>Update Komentar</h6>              
                    </div>
                    <div class="card-body">
                        <div class="card border-left-primary">
                            <div class="card-body">
                                <form action="/komentar/update/" method="POST">
                                @csrf
                                @method('PUT')
                                  <div class="form-group">
                                    <label for="exampleInputEmail1">Isi Komentar</label>
                                    <textarea class="form-control" id="isi" name="isi" rows="5">{{ $komentar->isi }}</textarea> 
                                    <input type="hidden" value="{{ $komentar->id }}" name="komentar_id">
                                  </div>
                                  <button type="submit" class="btn btn-primary">Update</button>
                                </form>
                            </div>
                        </div>
                    </div>   
                </div>
            </div>
        </div>
@endsection

