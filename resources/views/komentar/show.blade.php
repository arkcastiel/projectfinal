@extends('layout.master')

@section('title')
   Dashboard
@endsection

@section('content')
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary"><i class="fa fa-fire"></i>Posting</h6>              
                    </div>
                    <div class="card-body">
                        <div class="card border-left-primary">
                            <div class="card-body">
                                <p>{{ $post->isi }}</p>
                            </div>
                        </div>
                        <span><b>Komentar :</b></span> 
                        @if($post->komentar)
                            @foreach($post->komentar as $item_komentar)
                            <div class="alert alert-info" role="alert">
                              {{ $item_komentar->isi }}
                              <br>
                              <small><i>Oleh : {{ $item_komentar->user->name}}</i></small>
                              @if(Auth::user()->id == $item_komentar->user_id)
                                <br>
                                <a class= "btn btn-primary btn-sm" href="/komentar/{{ $item_komentar->id }}/edit"><i class="fa fa-edit"></i> Edit</a> |
                                <form action='/komentar/{{$item_komentar->id}}' method="POST" style="display: inline">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Delete</button>
                                </form>
                             @endif
                            </div>
                            @endforeach
                        @else
                        <span><i>Belum ada Komentar</i></span>
                        @endif
                    </div>   
                </div>
            </div>
        </div>
@endsection

